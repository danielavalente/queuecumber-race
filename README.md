Queuecumber Race

A group project for a Bootcamp challenge.
We created a "Cross Road" game in only 1 week with only a month and a half of bootcamp using Java.

Developers:
Dani Valente -> https://www.linkedin.com/in/danielavalentedeveloper/
João Skultetty -> https://www.linkedin.com/in/jo%C3%A3o-skultetty-0772871b3/?originalSubdomain=pt
Miles Montgomery -> https://www.linkedin.com/in/miles-montgomery-93528b168/
Paula Silveira -> https://www.linkedin.com/in/2020-paula-de-castro/
ZP Almeida -> https://www.linkedin.com/in/jos%C3%A9-pedro-almeida-1b19601a9/